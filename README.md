# qsv_dec

#### 介绍
测试基于FFMPEG 4.3的QSV解码性能
代码参考自FFMPEG官方例程[qsvdec.c](https://github.com/FFmpeg/FFmpeg/blob/master/doc/examples/qsvdec.c)

#### 软件架构
用cmake生成opencv/opencl/ffmpeg的开发环境，代码用于测试FFMPEG QSV解码的各个函数的性能
需要Windows10，VisualStudio和Intel的集成显卡

#### 安装教程

1.  OpenCV开发环境基于OpenVINO的opencv组件
2.  从Intel官网[Intel OpenCL SDK](https://software.intel.com/content/www/us/en/develop/tools/opencl-sdk/choose-download.html)下载安装Intel OpenCL SDK
3.  下载FFMPEG开发安装包，解压缩到本地目录

#### 使用说明

1.  打开VS的x64 Native Tools Command Prompt for VS 2017窗口
2.  运行"c:\Program Files (x86)\IntelSWTools\openvino\bin\setupvars.bat"初始化opencv开发环境
3.  将本项目下载到本地，进到根目录下，编辑CMakeLists.txt, 设置FFMPEG的路径

```
    set( FFMPEG_INCLUDES "C:/temp_20151027/ffmpeg-4.3.1-2020-11-08-full_build-shared/include" )
    set( FFMPEG_LIBRARY_PATHS "C:/temp_20151027/ffmpeg-4.3.1-2020-11-08-full_build-shared/lib" )
```

4.  运行命令，即可生成程序

```
cd build
cmake -G "Visual Studio 15 2017 Win64" -DCMAKE_BUILD_TYPE=Release ..
cmake --build . --config Release

```
5.  运行程序

```
cl-qsvdec.exe [input filename] [output filename]
```
[input filename]为要解码播放的文件  
[output filename]没用，随便输入一个文件名即可，程序会创建一个0byte的文件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

